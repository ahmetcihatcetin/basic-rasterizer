# Basic Rasterizer

Basic rasterizer which takes two input files containing camera and scene information as .txt files, and render an image by using forward rendering. The output is given as .ppm or .png file depending the OS used.
<br /> Sample usage:
<br />>_ make rasterizer
<br />>_ ./rasterizer inputs/culling_enabled_inputs/horse_and_mug/horse_and_mug_camera.txt inputs/culling_enabled_inputs/horse_and_mug/horse_and_mug_scene.txt
<br />
<br />This program has been developed by Ahmet Cihat Çetin and Hüseyin Emre Çekiç as a homework: semesterNo:20181-courseNo:5710477-Assignment#2