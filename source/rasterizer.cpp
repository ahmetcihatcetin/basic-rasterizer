#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include "hw2_types.h"
#include "hw2_math_ops.h"
#include "hw2_file_ops.h"
#include <iostream>


Camera cameras[100];
int numberOfCameras = 0;

Model models[1000];
int numberOfModels = 0;

Color colors[100000];
int numberOfColors = 0;

Translation translations[1000];
int numberOfTranslations = 0;

Rotation rotations[1000];
int numberOfRotations = 0;

Scaling scalings[1000];
int numberOfScalings = 0;

Vec3 vertices[100000];
int numberOfVertices = 0;

Color backgroundColor;

// backface culling setting, default disabled
int backfaceCullingSetting = 0;

Color **image;

typedef struct {
    double x, y, z, w;
    int colorId;
} Vec4;

typedef struct {
    double x, y;
    int colorId;
} Vec2;



/*
	Initializes image with background color
*/
void initializeImage(Camera cam) {
    int i, j;

    for (i = 0; i < cam.sizeX; i++)
        for (j = 0; j < cam.sizeY; j++) {
            image[i][j].r = backgroundColor.r;
            image[i][j].g = backgroundColor.g;
            image[i][j].b = backgroundColor.b;

        }
}

/*
	Transformations, culling, rasterization are done here.
	You can define helper functions inside this file (rasterizer.cpp) only.
	Using types in "hw2_types.h" and functions in "hw2_math_ops.cpp" will speed you up while working.
*/

// OUR ADDITIONS *************************************************************************************
// Our helpers:
double min(double x, double y, double z){
    double result=x;
    if (y<result) result=y;
    if (z<result) result=z;
    return result;
}

int min(int x, int y, int z){
    int result=x;
    if (y<result) result=y;
    if (z<result) result=z;
    return result;
}

int max(int x, int y, int z){
    int result=x;
    if (y>result) result=y;
    if (z>result) result=z;
    return result;
}

int max(double x, double y, double z){
    int result=x;
    if (y>result) result=y;
    if (z>result) result=z;
    return result;
}
void multiplyMatrix3x4WithVec4d(double r[3], double m[3][4], double v[4]){
    int i, j;
    double total;
    for (i = 0; i < 3; i++) {
        total = 0;
        for (j = 0; j < 4; j++)
            total += m[i][j] * v[j];
        r[i] = total;
    }
}

//power func for triangle rasterization:
double f_power(double x,double y,Vec2 v0,Vec2 v1){
    return x*(v0.y-v1.y)+y*(v1.x-v0.x)+v0.x*v1.y-v0.y*v1.x;
}

//MODELING TRANSFORMATIONS (HELPER)
//Translation:
void translating_matrix(double m[4][4], Translation t){
    double tmp_matrix[4][4];
    makeIdentityMatrix(tmp_matrix);

    tmp_matrix[0][3]=t.tx;
    tmp_matrix[1][3]=t.ty;
    tmp_matrix[2][3]=t.tz;

    double tmp_result[4][4];
    multiplyMatrixWithMatrix(tmp_result,tmp_matrix,m);
    makeIdentityMatrix(tmp_matrix);
    multiplyMatrixWithMatrix(m,tmp_matrix,tmp_result);
    // std::cout<<"Translate:"<<std::endl;
}

//Scaling:
void scaling_matrix(double m[4][4], Scaling s){
    double tmp_matrix[4][4];
    makeIdentityMatrix(tmp_matrix);
//    std::cout<<"check___inside"<<std::endl;
    tmp_matrix[0][0]=s.sx;
    tmp_matrix[1][1]=s.sy;
    tmp_matrix[2][2]=s.sz;

    double tmp_result[4][4];
    multiplyMatrixWithMatrix(tmp_result,tmp_matrix,m);
//    std::cout<<"tmp_result:"<<tmp_result[0][0]<<std::endl;
    makeIdentityMatrix(tmp_matrix);
    multiplyMatrixWithMatrix(m,tmp_matrix,tmp_result);
//    std::cout<<"m:"<<m[0][0]<<std::endl;
    // std::cout<<"Scale:"<<std::endl;
}

//Rotation:
void rotating_matrix(double m[4][4], Rotation r){
    //u vector:
    double teta=r.angle;
    double d=sqrt(r.uy*r.uy+r.uz*r.uz);
    double a=r.ux;
    double b=r.uy;
    double c=r.uz;

    double rotate_x_alfa[4][4]; //R_x(alfa)
    makeIdentityMatrix(rotate_x_alfa);
    rotate_x_alfa[1][1]=c/d;
    rotate_x_alfa[1][2]=(-b)/d;
    rotate_x_alfa[2][1]=b/d;
    rotate_x_alfa[2][2]=c/d;
    //R_x(alfa) computed!!!

    double rotate_x_minus_alfa[4][4];   //R_x(minus_alfa)
    makeIdentityMatrix(rotate_x_minus_alfa);
    rotate_x_minus_alfa[1][1]=c/d;
    rotate_x_minus_alfa[1][2]=b/d;
    rotate_x_minus_alfa[2][1]=(-b)/d;
    rotate_x_minus_alfa[2][2]=c/d;
    //R_x(minus_alfa) computed!

    double rotate_y_beta[4][4]; //R_y(beta)
    makeIdentityMatrix(rotate_y_beta);
    double length=sqrt(a*a+b*b+c*c);
    double tmp_var=sqrt(b*b+c*c);
    rotate_y_beta[0][0]=tmp_var/length;
    rotate_y_beta[0][2]=a/length;
    rotate_y_beta[2][1]=(-a)/length;
    rotate_y_beta[2][2]=tmp_var/length;
    //R_y(beta) computed!

    double rotate_y_minus_beta[4][4]; //R_y(minus_beta)
    makeIdentityMatrix(rotate_y_minus_beta);
    rotate_y_minus_beta[0][0]=tmp_var/length;
    rotate_y_minus_beta[0][2]=(-a)/length;
    rotate_y_minus_beta[2][1]=a/length;
    rotate_y_minus_beta[2][2]=tmp_var/length;

    double rotate_z_teta[4][4]; //R_z(teta)
    makeIdentityMatrix(rotate_z_teta);
    rotate_z_teta[0][0]=cos(teta*(M_PI/180.0));
    rotate_z_teta[0][1]=-sin(teta*(M_PI/180.0));
    rotate_z_teta[1][0]=sin(teta*(M_PI/180.0));
    rotate_z_teta[1][1]=cos(teta*(M_PI/180.0));
    //R_z(teta) computed!

    double composite_1[4][4];
    multiplyMatrixWithMatrix(composite_1,rotate_y_minus_beta,rotate_x_alfa);
    double composite_2[4][4];
    multiplyMatrixWithMatrix(composite_2,rotate_z_teta,composite_1);
    double composite_3[4][4];
    multiplyMatrixWithMatrix(composite_3,rotate_y_beta,composite_2);
    double composite_4[4][4];
    multiplyMatrixWithMatrix(composite_4,rotate_x_minus_alfa,composite_3);

    double tmp_identity[4][4];
    makeIdentityMatrix(tmp_identity);
    multiplyMatrixWithMatrix(composite_1,composite_4,m);
    multiplyMatrixWithMatrix(m,tmp_identity,composite_1);



}
//END OF MODEL TRANSFORMATIONS

//CAMERA TRANSFORMATION:
void camera_transformation(double m[4][4],Camera cam){


    double tmp_matrix[4][4];
    makeIdentityMatrix(tmp_matrix);

    tmp_matrix[0][3]=-cam.pos.x;
    tmp_matrix[1][3]=-cam.pos.y;
    tmp_matrix[2][3]=-cam.pos.z;


    double tmp_M[4][4];
    makeIdentityMatrix(tmp_M);
    tmp_M[0][0]=cam.u.x;
    tmp_M[0][1]=cam.u.y;
    tmp_M[0][2]=cam.u.z;

    tmp_M[1][0]=cam.v.x;
    tmp_M[1][1]=cam.v.y;
    tmp_M[1][2]=cam.v.z;

    tmp_M[2][0]=cam.w.x;
    tmp_M[2][1]=cam.w.y;
    tmp_M[2][2]=cam.w.z;



    /*
    std::cout<<"cam_u:"<<cam.u.x<<","<<cam.u.y<<","<<cam.u.z<<std::endl;
    std::cout<<"cam_v:"<<cam.v.x<<","<<cam.v.y<<","<<cam.v.z<<std::endl;
    std::cout<<"cam_w:"<<cam.w.x<<","<<cam.w.y<<","<<cam.w.z<<std::endl;
    */


    double result[4][4];
    multiplyMatrixWithMatrix(result,tmp_M,tmp_matrix);
    multiplyMatrixWithMatrix(tmp_M,result,m);
    makeIdentityMatrix(result);
    multiplyMatrixWithMatrix(m,tmp_M,result);
}

//PERSPECTIVE PROJECTION:
void perspective_projection(double m[4][4], Camera cam){
    m[0][0]=(2*cam.n)/(cam.r-cam.l);
    m[1][1]=(2*cam.n)/(cam.t-cam.b);

    m[0][2]=(cam.r+cam.l)/(cam.r-cam.l);
    m[1][2]=(cam.t+cam.b)/(cam.t-cam.b);
    m[2][2]=-(cam.f+cam.n)/(cam.f-cam.n);

    m[2][3]=-(2*cam.f*cam.n)/(cam.f-cam.n);

    m[3][2]=-1;
    m[3][3]=0;
    // std::cout<<"projection:"<<std::endl;
}

//VIEWPORT TRANSFORMATION:
void viewport_transformation(double m[3][4], Camera cam){
    m[0][0]=(cam.sizeX)/2;
    m[1][1]=(cam.sizeY)/2;

    m[0][3]=(cam.sizeX-1)/2;
    m[1][3]=(cam.sizeY-1)/2;

    m[2][2]=1;
    m[2][3]=1;

    m[0][1]=0;
    m[0][2]=0;
    m[1][0]=0;
    m[1][2]=0;
    m[2][0]=0;
    m[2][1]=0;

    // std::cout<<"viewport:"<<std::endl;
}


//DRAW FUNCTION:
void draw(int x, int y, Color color){
    image[x][y]=color;
}
//RASTERIZATION FUNCTIONS:

void line_raster(Vec2 vertex_0,Vec2 vertex_1)
{
    Color c_0=colors[vertex_0.colorId];
    Color c_1=colors[vertex_1.colorId];
    if (vertex_1.x-vertex_0.x>0) //////////SECTOR 1
    {

        double slope_01=(vertex_1.y-vertex_0.y)/(vertex_1.x-vertex_0.x);

        if (slope_01>=0&&slope_01<=1) //zone1
        { //zone_1 0-45

//                        std::cout<<"xxx"<<std::endl;
            int y=vertex_0.y;
            double d=(vertex_0.y-vertex_1.y)+0.5*(vertex_1.x-vertex_0.x);
            Color color=c_0;
            Color d_c;
            d_c.r=(c_1.r-c_0.r)/(vertex_1.x-vertex_0.x);
            d_c.g=(c_1.g-c_0.g)/(vertex_1.x-vertex_0.x);
            d_c.b=(c_1.b-c_0.b)/(vertex_1.x-vertex_0.x);
//                        std::cout<<"xxxdeee"<<std::endl;
            for (int x=vertex_0.x; x<vertex_1.x; x++)
            {
//                            std::cout<<x<<std::endl;
                draw(x,y,color);
                if (d<0)
                {   //choose NE
                    y+=1;
                    d+=(vertex_0.y-vertex_1.y)+(vertex_1.x-vertex_0.x);
                }
                else
                    d+=(vertex_0.y-vertex_1.y);

                //color+=d_c:
                color.r=color.r+d_c.r;
                color.g=color.g+d_c.g;
                color.b=color.b+d_c.b;
            }
        }
        else if(slope_01>1) //zone 2
        {
            int x=vertex_0.x;
            double d=(vertex_1.x-vertex_0.x)+0.5*(vertex_0.y-vertex_1.y);
            Color color=c_0;
            Color d_c;
            d_c.r=(c_1.r-c_0.r)/(vertex_1.y-vertex_0.y);
            d_c.g=(c_1.g-c_0.g)/(vertex_1.y-vertex_0.y);
            d_c.b=(c_1.b-c_0.b)/(vertex_1.y-vertex_0.y);

            for (int y=vertex_0.y; y<vertex_1.y; y++)
            {

                draw(x,y,color);
                if (d>0)
                {   //choose NE
                    x+=1;
                    d+=(vertex_0.y-vertex_1.y)+(vertex_1.x-vertex_0.x);
                }
                else
                    d+=(vertex_1.x-vertex_0.x);

                //color+=d_c:
                color.r=color.r+d_c.r;
                color.g=color.g+d_c.g;
                color.b=color.b+d_c.b;
            }
        }
        else if(slope_01>-1 && slope_01<0) //zone 8
        {
            line_raster(vertex_1,vertex_0);

        }
        else if(slope_01<-1)
        {
            line_raster(vertex_1,vertex_0);
        }








    }
    else if(vertex_1.x-vertex_0.x<0)
    {
        double slope_01=(vertex_1.y-vertex_0.y)/(vertex_1.x-vertex_0.x);

        if(slope_01<-1)  //zone3
        {
            int x=vertex_0.x;
            double d=(vertex_1.x-vertex_0.x)+0.5*(vertex_1.y-vertex_0.y);
            Color color=c_0;
            Color d_c;
            d_c.r=(c_1.r-c_0.r)/(vertex_1.y-vertex_0.y);
            d_c.g=(c_1.g-c_0.g)/(vertex_1.y-vertex_0.y);
            d_c.b=(c_1.b-c_0.b)/(vertex_1.y-vertex_0.y);

            for (int y=vertex_0.y; y<vertex_1.y; y++)
            {

                draw(x,y,color);
                if (d<0)
                {   //choose W
                    x-=1;
                    d+=(vertex_1.y-vertex_0.y)+(vertex_1.x-vertex_0.x);
                }
                else
                    d+=(vertex_1.x-vertex_0.x);

                //color+=d_c:
                color.r=color.r+d_c.r;
                color.g=color.g+d_c.g;
                color.b=color.b+d_c.b;
            }
        }
        else if(slope_01>-1 && slope_01<0) // zone4
        {
            int y=vertex_0.y;
            double d=(vertex_1.y-vertex_0.y)+0.5*(vertex_1.x-vertex_0.x);
            Color color=c_0;
            Color d_c;
            d_c.r=(c_1.r-c_0.r)/(vertex_0.x-vertex_1.x);
            d_c.g=(c_1.g-c_0.g)/(vertex_0.x-vertex_1.x);
            d_c.b=(c_1.b-c_0.b)/(vertex_0.x-vertex_1.x);
//                        std::cout<<"xxxdeee"<<std::endl;
            for (int x=vertex_0.x; x>vertex_1.x; x--)
            {
//                            std::cout<<x<<std::endl;
                draw(x,y,color);
                if (d>0)
                {   //choose NE
                    y+=1;
                    d+=(vertex_1.y-vertex_0.y)+(vertex_1.x-vertex_0.x);
                }
                else
                    d+=(vertex_1.y-vertex_0.y);

                //color+=d_c:
                color.r=color.r+d_c.r;
                color.g=color.g+d_c.g;
                color.b=color.b+d_c.b;
            }
        }
        else if(slope_01>1)
        {
             line_raster(vertex_1,vertex_0);
        }
        else if(slope_01>0 && slope_01<1)
        {
             line_raster(vertex_1,vertex_0);
        }

    }

}
//********************************************************************

void forwardRenderingPipeline(Camera cam) {
    // TODO: IMPLEMENT HERE

    //CAMERA TRANSFORMATIONS
    double cam_matrix[4][4];
    makeIdentityMatrix(cam_matrix);
    //Apply cam transformation:
    camera_transformation(cam_matrix, cam);
    //Camera matrix computed!!!


    //PERSPECTIVE PROJECTION:
    double perspective_matrix[4][4];
    makeIdentityMatrix(perspective_matrix);
    perspective_projection(perspective_matrix,cam);
    //Perspective matrix computed!

    //VIEWPORT TRANSFORMATION:
    double viewport_matrix[3][4];
    //makeIdentityMatrix(viewport_matrix);
    viewport_transformation(viewport_matrix,cam);



    //Iteration 1: iterate over models: !!!!!!!!!!!!!   //MAIN ITERATION
    for (int i=0; i<numberOfModels; i++){

        //MODELING TRANSFORMATIONS:
        double modeling_matrix[4][4];//Modeling matrix declared to be computed!!!
        makeIdentityMatrix(modeling_matrix);

        //Iteration 2: iterate over transformations:
        for (int j=0; j<models[i].numberOfTransformations; j++){
            if (models[i].transformationTypes[j]=='t'){

                translating_matrix(modeling_matrix,translations[models[i].transformationIDs[j]]);
            }
            else if (models[i].transformationTypes[j]=='s'){
//                std::cout<<"check___"<<std::endl;
                scaling_matrix(modeling_matrix,scalings[models[i].transformationIDs[j]]);
//                std::cout<<"modeling_matrix0_00:"<<modeling_matrix[0][0]<<std::endl;
//                std::cout<<"modeling_matrix0_11:"<<modeling_matrix[1][1]<<std::endl;
//                std::cout<<"modeling_matrix0_22:"<<modeling_matrix[2][2]<<std::endl;
            }
            else {
//                std::cout<<"modeling_matrix0_00_before r_x:"<<modeling_matrix[0][0]<<std::endl;
//                std::cout<<"modeling_matrix0_11_before r_y:"<<modeling_matrix[1][1]<<std::endl;
//                std::cout<<"modeling_matrix0_22_before r_z:"<<modeling_matrix[2][2]<<std::endl;
                rotating_matrix(modeling_matrix,rotations[models[i].transformationIDs[j]]);
//                std::cout<<"modeling_matrix0_00_after r_x:"<<modeling_matrix[0][0]<<std::endl;
//                std::cout<<"modeling_matrix0_11_after r_y:"<<modeling_matrix[1][1]<<std::endl;
//                std::cout<<"modeling_matrix0_22_after r_z:"<<modeling_matrix[2][2]<<std::endl;
            }
        }
        //END OF MODELING TRANSFORMATIONS
        //Modeling matrix computed!

        //Now calculate vertex_processing matrix:
        double tmp[4][4];
        //makeIdentityMatrix(tmp);
//        std::cout<<"modeling_matrix_00:"<<modeling_matrix[0][0]<<std::endl;
//        std::cout<<"modeling_matrix_11:"<<modeling_matrix[1][1]<<std::endl;
//        std::cout<<"modeling_matrix_22:"<<modeling_matrix[2][2]<<std::endl;

//        std::cout<<"----------------------"<<std::endl;


        multiplyMatrixWithMatrix(tmp,cam_matrix,modeling_matrix);
//        std::cout<<"tmp:"<<tmp[0][0]<<std::endl;
        double result[4][4];    // to be multiplied with vertex_vector!!!!!!!!!
        //makeIdentityMatrix(result);
        multiplyMatrixWithMatrix(result,perspective_matrix,tmp);    //result 4x4 matrix!
//        std::cout<<"result:"<<result[0][0]<<std::endl;
        //create an array for the processed vertices not to reprocess them: !!!!
        int is_processed[numberOfVertices+1];   //We'll use this array during vertex processing!!!
        //initialize all of them to 0's:
        for (int init_index=0; init_index<=numberOfVertices; init_index++)
            is_processed[init_index]=0;

        //create an array for the positions of the processed vectors: array of Vec3s:
        Vec4 culling_vertices[numberOfVertices+1];
        for (int i=1; i<=numberOfVertices; i++){
                culling_vertices[i].x=vertices[i].x;
                culling_vertices[i].y=vertices[i].y;
                culling_vertices[i].z=vertices[i].z;
                culling_vertices[i].w=1;
                culling_vertices[i].colorId=vertices[i].colorId;
                 //initialize processed vertices to vertices!
        }
        culling_vertices[0].x=1;
        culling_vertices[0].y=1;
        culling_vertices[0].z=1;
        culling_vertices[0].w=1;
        culling_vertices[0].colorId=0;
        //Now multply result with vertices:
        //Iteration 3: iterate over triangles of a model:
        // std::cout<<"after culling initialize:"<<std::endl;
        for (int k=0; k<models[i].numberOfTriangles; k++){
            int cur_triangle[3];
            cur_triangle[0]=models[i].triangles[k].vertexIds[0];
            cur_triangle[1]=models[i].triangles[k].vertexIds[1];
            cur_triangle[2]=models[i].triangles[k].vertexIds[2];

            for (int tmp=0; tmp<3; tmp++){
                int vertex_index=cur_triangle[tmp];
                if (!is_processed[vertex_index]){    //process vertex if it hasn't been processed
                    Vec3 cur_vertex=vertices[vertex_index];
                    double vertex_vector[4];    //vertex vector to be backfaced!!!
                    vertex_vector[0]=cur_vertex.x;
                    vertex_vector[1]=cur_vertex.y;
                    vertex_vector[2]=cur_vertex.z;
                    vertex_vector[3]=1;

                   // std::cout<<vertex_vector[0]<<","<<vertex_vector[1]<<","<<vertex_vector[2]<<std::endl;


                    double immediate_vertex[4];
                    multiplyMatrixWithVec4d(immediate_vertex,result,vertex_vector);
                    //UPDATE processed vertex: !!!!!!
                    culling_vertices[vertex_index].x=immediate_vertex[0];
                    culling_vertices[vertex_index].y=immediate_vertex[1];
                    culling_vertices[vertex_index].z=immediate_vertex[2];
                    culling_vertices[vertex_index].w=immediate_vertex[3];
//                    std::cout<<immediate_vertex[0]<<","<<immediate_vertex[1]<<","<<immediate_vertex[2]<<","<<immediate_vertex[3]<<std::endl;
//                    std::cout<<"---"<<std::endl;
                    is_processed[vertex_index]=1;
                }
            }
        }


//        BACKFACE CULLING:
//        Iteration 4: iterate over triangles of a model (MAIN ITERATION FOR THE BACKFACE DISCUSSIONS):
//         std::cout<<"before backface:"<<std::endl;
        for(int i=1;i<=numberOfVertices;i++)
        {
            culling_vertices[i].x/=culling_vertices[i].w;
            culling_vertices[i].y/=culling_vertices[i].w;
            culling_vertices[i].z/=culling_vertices[i].w;
            culling_vertices[i].w/=culling_vertices[i].w;
        }

        int to_be_rasterized[models[i].numberOfTriangles]; //create an array to store info that whether or not the vertices are gonna be rasterized!
        for (int k=0; k<models[i].numberOfTriangles; k++){
            to_be_rasterized[k]=1;
        }
        if (backfaceCullingSetting){
            for (int k=0; k<models[i].numberOfTriangles; k++){
                Vec3 vertex_A;
                Vec3 vertex_B;
                Vec3 vertex_C;

                vertex_A.x=culling_vertices[(models[i].triangles[k].vertexIds[0])].x;
                vertex_A.y=culling_vertices[(models[i].triangles[k].vertexIds[0])].y;
                vertex_A.z=culling_vertices[(models[i].triangles[k].vertexIds[0])].z;

                vertex_B.x=culling_vertices[(models[i].triangles[k].vertexIds[1])].x;
                vertex_B.y=culling_vertices[(models[i].triangles[k].vertexIds[1])].y;
                vertex_B.z=culling_vertices[(models[i].triangles[k].vertexIds[1])].z;

                vertex_C.x=culling_vertices[(models[i].triangles[k].vertexIds[2])].x;
                vertex_C.y=culling_vertices[(models[i].triangles[k].vertexIds[2])].y;
                vertex_C.z=culling_vertices[(models[i].triangles[k].vertexIds[2])].z;



//                Vec3 triangle_center=addVec3(vertex_A,addVec3(vertex_B,vertex_C));
//                triangle_center=multiplyVec3WithScalar(triangle_center,(1/3));


//                std::cout<<vertex_A.x<<","<<vertex_A.y<<","<<vertex_A.z<<std::endl;
//                std::cout<<".........."<<std::endl;
                Vec3 v=vertex_A;
                v.x=-v.x;
                v.y=-v.y;
                v.z=-v.z;//vector v!
//                std::cout<<v.x<<","<<v.y<<","<<v.z<<std::endl;
                //v=normalizeVec3(v);
                //now calculate the normal of the current triangle:
                Vec3 n=crossProductVec3(subtractVec3(vertex_A,vertex_B),subtractVec3(vertex_A,vertex_C));

                //n=normalizeVec3(n);
                double test=dotProductVec3(v,n);
                //std::cout<<test<<std::endl;
                //Now backface test:
                if (test>=0)
                    to_be_rasterized[k]=0;
            }

        }
//        std::cout<<"|||||||"<<std::endl;


        //std::cout<<"after backface:"<<std::endl;
        //APPLY PERSPECTIVE DIVIDE:
//         for(int i=1;i<=numberOfVertices;i++)
//         {
//             std::cout<<culling_vertices[i].x<<","<<culling_vertices[i].y<<","<<culling_vertices[i].z<<std::endl;
//             std::cout << "-------------" << '\n';
//         }



//         std::cout<<"after perspective divide:"<<std::endl;
//        for(int i=1;i<=numberOfVertices;i++)
//        {
//            std::cout<<culling_vertices[i].x<<","<<culling_vertices[i].y<<","<<culling_vertices[i].z<<std::endl;
//            std::cout << "-------------" << '\n';
//        }

        //APPLY VIEWPORT:
        Vec2 pixel_coords[numberOfVertices+1];  //FROM NOW ON WE'LL USE THIS COORDS IN RASTERIZATION!!!
        for (int l=1; l<=numberOfVertices; l++){
                pixel_coords[l].x=culling_vertices[l].x;
                pixel_coords[l].y=culling_vertices[l].y;
                pixel_coords[l].colorId=culling_vertices[l].colorId;
                 //initialize processed vertices to vertices!
        }
        pixel_coords[0].x=1;
        pixel_coords[0].y=1;
        pixel_coords[0].colorId=1;



        for(int k=0;k<models[i].numberOfTriangles;k++)
        {
            for(int m=0;m<3;m++)
            {
                double tmp_vertex[4];
                tmp_vertex[0]=culling_vertices[(models[i].triangles[k].vertexIds[m])].x;
                tmp_vertex[1]=culling_vertices[(models[i].triangles[k].vertexIds[m])].y;
                tmp_vertex[2]=culling_vertices[(models[i].triangles[k].vertexIds[m])].z;
                tmp_vertex[3]=culling_vertices[(models[i].triangles[k].vertexIds[m])].w;

                double result[3];

                multiplyMatrix3x4WithVec4d(result,viewport_matrix,tmp_vertex); //result is 3x1 vector!


                pixel_coords[(models[i].triangles[k].vertexIds[m])].x=result[0];
                pixel_coords[(models[i].triangles[k].vertexIds[m])].y=result[1];
                pixel_coords[(models[i].triangles[k].vertexIds[m])].colorId=culling_vertices[(models[i].triangles[k].vertexIds[m])].colorId;

            }

        }
//        for(int i=1;i<=numberOfVertices;i++)
//               {
//                   std::cout<<culling_vertices[i].x<<","<<culling_vertices[i].y<<","<<culling_vertices[i].z<<","<<culling_vertices[i].w <<std::endl;
//                   std::cout<<pixel_coords[i].x<<","<<pixel_coords[i].y<<std::endl;
//                   std::cout << "-------------" << '\n';
//               }
//        std::cout<<"after viewport_transformation:"<<std::endl;
        //APPLY RASTERIZATION:
        if (models[i].type==0){ //LINE RASTERIZATION!!!
            //std::cout<<"LINE:"<<std::endl;
            //Apply midpoint alg: 8 cases:
            for (int k=0; k<models[i].numberOfTriangles; k++){
                if (!to_be_rasterized[k]) continue; //discard backfacing triangles!!!


                Vec2 vertex_0=pixel_coords[(models[i].triangles[k].vertexIds[0])];




                Vec2 vertex_1=pixel_coords[(models[i].triangles[k].vertexIds[1])];


                Vec2 vertex_2=pixel_coords[(models[i].triangles[k].vertexIds[2])];


                line_raster(vertex_0,vertex_1);
                line_raster(vertex_1,vertex_2);
                line_raster(vertex_2,vertex_0);




            }
        }

        else {  //TRIANGLE RASTERIZATION!!!!
//             std::cout<<"Triangle rast:"<<std::endl;
//             std::cout<<"___________________"<<std::endl;
            for (int k=0; k<models[i].numberOfTriangles; k++){
                if(to_be_rasterized[k]==0) continue;


                Vec2 vertex_0=pixel_coords[(models[i].triangles[k].vertexIds[0])];



                Vec2 vertex_1=pixel_coords[(models[i].triangles[k].vertexIds[1])];


                Vec2 vertex_2=pixel_coords[(models[i].triangles[k].vertexIds[2])];




                Color c_0=colors[vertex_0.colorId];
                Color c_1=colors[vertex_1.colorId];
                Color c_2=colors[vertex_2.colorId];
//                std::cout<<"vertex_0:"<<vertex_0.x<<","<<vertex_0.y<<std::endl;
//                std::cout<<"c_0:"<<c_0.r<<","<<c_0.g<<","<<c_0.b<<std::endl;

//                std::cout<<"vertex_0:"<<vertex_1.x<<","<<vertex_1.y<<std::endl;
//                std::cout<<"c_1:"<<c_1.r<<","<<c_1.g<<","<<c_1.b<<std::endl;

//                std::cout<<"vertex_2:"<<vertex_0.x<<","<<vertex_2.y<<std::endl;
//                std::cout<<"c_2:"<<c_2.r<<","<<c_2.g<<","<<c_2.b<<std::endl;
//                std::cout<<"||||||"<<std::endl;
//                std::cout<<std::endl;
                int y=min(vertex_0.y,vertex_1.y,vertex_2.y);
                int x=min(vertex_0.x,vertex_1.x,vertex_2.x);
                for (;y<=max(vertex_0.y,vertex_1.y,vertex_2.y);y++){
//                         std::cout<<"|||||--NEW Y COORDINATE--|||||"<<std::endl;
                         x=min(vertex_0.x,vertex_1.x,vertex_2.x);
                    for (;x<=max(vertex_0.x,vertex_1.x,vertex_2.x);x++){
                        // std::cout<<"iteration x:"<<std::endl;
                       // if(!f_power(vertex_0.x,vertex_0.y,vertex_1,vertex_2)) continue;
                        double alfa=f_power(x,y,vertex_1,vertex_2)/f_power(vertex_0.x,vertex_0.y,vertex_1,vertex_2);
//                         std::cout<<"alfa:"<<alfa<<std::endl;
                        //if(!f_power(vertex_1.x,vertex_1.y,vertex_2,vertex_0)) continue;
                        double beta=f_power(x,y,vertex_2,vertex_0)/f_power(vertex_1.x,vertex_1.y,vertex_2,vertex_0);
//                         std::cout<<"beta:"<<beta<<std::endl;
                        //if(!f_power(vertex_2.x,vertex_2.y,vertex_0,vertex_1)) continue;
                        double gamma=f_power(x,y,vertex_0,vertex_1)/f_power(vertex_2.x,vertex_2.y,vertex_0,vertex_1);
//                         std::cout<<"gamma:"<<gamma<<std::endl;
//                        std::cout<<"x="<<x<<" y="<<y<<" "<<std::endl;
//                        std::cout<<"------"<<std::endl;

                        if (alfa>=0 && beta>=0 && gamma>=0 ){
                            Color color;
                            color.r=alfa*c_0.r+beta*c_1.r+gamma*c_2.r;
                            color.g=alfa*c_0.g+beta*c_1.g+gamma*c_2.g;
                            color.b=alfa*c_0.b+beta*c_1.b+gamma*c_2.b;
//                            std::cout<<alfa<<","<<beta<<","<<gamma<<std::endl;
//                             std::cout<<"c_0="<<c_0.r<<" "<<c_0.g<<" "<<c_0.b<<" "<<std::endl;
//                             std::cout<<"c_0="<<c_1.r<<" "<<c_1.g<<" "<<c_1.b<<" "<<std::endl;
//                             std::cout<<"c_0="<<c_2.r<<" "<<c_2.g<<" "<<c_2.b<<" "<<std::endl;
//                             std::cout<<"r="<<color.r<<" g="<<color.g<<" b="<<color.b<<" "<<std::endl;
//                             std::cout<<"x="<<x<<" y="<<y<<" "<<std::endl;
//                             std::cout<<"-------- "<<std::endl;
                             if(x>=0 && x<cam.sizeX && y>=0 && y<cam.sizeY)
                                    draw(x,y,color);

                        }

                    }
                }
            }

        }

    }
}


    /*
    //Apply translations:
    for (int i=0; i<numberOfTranslations; i++){
        translating_matrix(modeling_matrix, translations[i]);
    }

    //Apply scaling:
    for (int i=0; i<numberOfScalings; i++){
        scaling_matrix(modeling_matrix, scalings[i]);
    }

    //Apply rotates:
    for (int i=0; i<numberOfRotations; i++){
        rotating_matrix(modeling_matrix, rotations[i]);
    }
    */
    //Modeling matrix computed!












int main(int argc, char **argv) {
    int i, j;

    if (argc < 2) {
        std::cout << "Usage: ./rasterizer <scene file> <camera file>" << std::endl;
        return 1;
    }

    // read camera and scene files
    readSceneFile(argv[1]);
    readCameraFile(argv[2]);

    image = 0;

    for (i = 0; i < numberOfCameras; i++) {

        // allocate memory for image
        if (image) {
			for (j = 0; j < cameras[i].sizeX; j++) {
		        delete image[j];
		    }

			delete[] image;
		}

        image = new Color*[cameras[i].sizeX];

        if (image == NULL) {
            std::cout << "ERROR: Cannot allocate memory for image." << std::endl;
            exit(1);
        }

        for (j = 0; j < cameras[i].sizeX; j++) {
            image[j] = new Color[cameras[i].sizeY];
            if (image[j] == NULL) {
                std::cout << "ERROR: Cannot allocate memory for image." << std::endl;
                exit(1);
            }
        }


        // initialize image with basic values
        initializeImage(cameras[i]);

        // do forward rendering pipeline operations
        forwardRenderingPipeline(cameras[i]);

        // generate PPM file
        writeImageToPPMFile(cameras[i]);

        // Converts PPM image in given path to PNG file, by calling ImageMagick's 'convert' command.
        // Notice that os_type is not given as 1 (Ubuntu) or 2 (Windows), below call doesn't do conversion.
        // Change os_type to 1 or 2, after being sure that you have ImageMagick installed.
        convertPPMToPNG(cameras[i].outputFileName, 99);
    }

    return 0;

}
